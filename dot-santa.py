#!/usr/bin/env python3

import datetime
import os
import random
import tomllib


def shuffled_correctly(data: list, participants: list) -> bool:
    for i in range(len(participants)):
        # Check for sorting
        if participants[i] == data[i].get("name"):
            return False
        if participants[i] in data[i].get("avoids", []):
            return False
    return True


if __name__ == "__main__":
    path = ""
    entries = []
    with open("santa.toml", "rb") as f:
        data = tomllib.load(f)
        entries = data.get("entry", [])
        path = data.get("path", str(datetime.date.today().year)).strip().rstrip("\\/")
    original_names = list(map(lambda x: x.get("name"), entries))

    if not entries:
        print("No participants found.")
        exit(1)

    # Shuffle until we find a working solution
    while not shuffled_correctly(entries, original_names):
        random.shuffle(entries)

    # Write files
    if not os.path.exists(path):
        os.makedirs(path)
    for i in range(len(original_names)):
        with open(path + "/" + original_names[i] + ".txt", "w") as file:
            file.write("Dear " + original_names[i] + ",")
            file.write("\nYou're the secret santa for: " + entries[i].get("name"))
            if wishlist := entries[i].get("wishlist"):
                file.write("\nWishlist: " + ", ".join(wishlist))
            file.write("\n\nMerry Christmas!")
            file.write("\n(Created by https://gitlab.com/WuerfelDev/dot-santa)")
    print("Wrote files to '" + path + "/'. Merry Christmas!")
