# Dot-Santa

![generated image of secret santa](./secret-santa.png)

Secret santa generator that saves results as files (so that you as organizer don't see it).

**Attention**: Impossible inputs will result in a endless loop.

Why that name? [read this](https://en.wikipedia.org/wiki/Hidden_file_and_hidden_directory#Unix_and_Unix-like_environments)

Read the code, it's not a lot
